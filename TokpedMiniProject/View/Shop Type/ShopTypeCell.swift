//
//  ShopTypeCell.swift
//  TokpedMiniProject
//
//  Created by Abiyyu Alifandin on 3/12/19.
//  Copyright © 2019 Abiyyu Alifandin. All rights reserved.
//

import UIKit

class ShopTypeCell: UICollectionViewCell {
    @IBOutlet weak var shopTypeTitle: UILabel!
    
    @IBOutlet weak var close: UIButton!
}
