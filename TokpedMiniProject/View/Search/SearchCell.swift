//
//  SearchCell.swift
//  TokpedMiniProject
//
//  Created by Abiyyu Alifandin on 3/10/19.
//  Copyright © 2019 Abiyyu Alifandin. All rights reserved.
//

import UIKit

class SearchCell: UICollectionViewCell {
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
}
