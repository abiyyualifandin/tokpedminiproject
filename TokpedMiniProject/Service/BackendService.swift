//
//  BackendService.swift
//  TokpedMiniProject
//
//  Created by Abiyyu Alifandin on 3/9/19.
//  Copyright © 2019 Abiyyu Alifandin. All rights reserved.
//

import UIKit
import Alamofire

class BackendService {
    func getProduct(min: String, max: String, wholesale: String, official: String, fshop:String, rows: String){
        Alamofire.request(URL(string: "https://ace.tokopedia.com/search/v2.5/product?q=samsung&pmin=\(min)&pmax=\(max)&wholesale=\(wholesale)&official=\(official)&fshop=\(fshop)&start=0&rows=\(rows)")!)

            .validate()
            .response { (response) in
                
                print("Request: \(String(describing: response.request))")
                print("Response: \(String(describing: response.response))")
                print("Error: \(String(describing: response.error))")
                
                if let data = response.data {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: [])
                        print(json)
                        let jsonData = try JSONSerialization.data(withJSONObject:json)
                        let result = try JSONDecoder().decode(Product.Product.self, from: jsonData)
                        Product.listProduct = result.data
                    } catch {
                        print("Error: ", error)
                    }
                }
        }
    }
}


