//
//  ViewController.swift
//  TokpedMiniProject
//
//  Created by Abiyyu Alifandin on 3/9/19.
//  Copyright © 2019 Abiyyu Alifandin. All rights reserved.
//

import UIKit
import Alamofire
import Foundation

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var searchCollectionView: UICollectionView!
    @IBOutlet weak var filter: UIButton!
    public static var min : String = "100"
    public static var max : String = "8000000"
    public static var wholesale : String = "true"
    public static var official : String = "true"
    public static var fshop : String = "2"
    var fetchingMore = false
    var rows: Int = 10
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Product.listProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = searchCollectionView.dequeueReusableCell(withReuseIdentifier: "SearchCell", for: indexPath) as! SearchCell
        if Product.listProduct.count == 1 {
            let url = URL(string: Product.listProduct[0].imageURI)
            let data = try? Data(contentsOf: url!)
            if let imageData = data {
                cell.photo.image = UIImage(data: imageData)
            }
            
            cell.productName.text = Product.listProduct[0].name
            cell.productPrice.text = Product.listProduct[0].price
        }
        else{
            let url = URL(string: Product.listProduct[indexPath.row].imageURI)
            let data = try? Data(contentsOf: url!)
            if let imageData = data {
                cell.photo.image = UIImage(data: imageData)
            }
            
            cell.productName.text = Product.listProduct[indexPath.row].name
            cell.productPrice.text = Product.listProduct[indexPath.row].price
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = UIScreen.main.bounds.width
        return CGSize(width: width/2 - 15, height: 265)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Search"

        searchCollectionView.delegate = self
        searchCollectionView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        refreshData()
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.searchCollectionView.reloadData()
            }
        }
        rows=10
    }
    
    func refreshData() {
        let backend = BackendService()
        backend.getProduct(min: SearchViewController.min,max: SearchViewController.max,wholesale: SearchViewController.wholesale,official: SearchViewController.official,fshop: SearchViewController.fshop, rows: String(rows) )
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - (scrollView.frame.height * 4) {
            if !self.fetchingMore{
                self.beginBatchFetc()
            }
            print("Semangat")
        }
    }
    
    func beginBatchFetc() {
        fetchingMore = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.rows += 10
            self.refreshData()
            self.fetchingMore = false
            DispatchQueue.global(qos: .userInitiated).async {
                DispatchQueue.main.async {
                    self.searchCollectionView.reloadData()
                }
            }
        }
    }
}


