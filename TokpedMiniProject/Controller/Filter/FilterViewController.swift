//
//  FilterViewController.swift
//  TokpedMiniProject
//
//  Created by Abiyyu Alifandin on 3/9/19.
//  Copyright © 2019 Abiyyu Alifandin. All rights reserved.
//

import UIKit
import WARangeSlider

class FilterViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var minimumPrice: UILabel!
    @IBOutlet weak var maximumPrice: UILabel!
    @IBOutlet weak var wholeSale: UISwitch!
    @IBOutlet weak var apply: UIButton!
    @IBOutlet weak var shopTypeCollectionView: UICollectionView!
    @IBOutlet weak var slideView: UIView!
    
    let rangeSlider = RangeSlider(frame: CGRect.zero)
    var text: String = ""
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ShopType.listShopType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = shopTypeCollectionView.dequeueReusableCell(withReuseIdentifier: "ShopTypeCell", for: indexPath) as! ShopTypeCell
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.gray.cgColor
        
        cell.shopTypeTitle.text = ShopType.listShopType[indexPath.row]
        text = ShopType.listShopType[indexPath.row]
        cell.close.addTarget(self, action: #selector(closeTouch), for: .touchUpInside)
        
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Filter"
        
        slideView.addSubview(rangeSlider)
        
        let width = view.bounds.width - 35.0
        rangeSlider.frame = CGRect(x: 20, y: 20, width: width, height: 30.0)
        rangeSlider.trackHighlightTintColor = UIColorFromRGB(rgbValue: 4240720)
        rangeSlider.addTarget(self, action: #selector(rangeSliderValueChanged(_:)), for: .valueChanged)
        
        wholeSale.addTarget(self, action: #selector(switchIsChanged(_:)), for: .valueChanged)
        SearchViewController.wholesale = "true"
        
        apply.addTarget(self, action: #selector(applyFilter), for: .touchUpInside)
        
        shopTypeCollectionView.delegate = self
        shopTypeCollectionView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.shopTypeCollectionView.reloadData()
            }
        }
    }
    
    @objc func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        minimumPrice.text = "Rp. \(String(Int(rangeSlider.lowerValue)))"
        maximumPrice.text = "Rp. \(String(Int(rangeSlider.upperValue)))"
    }
    
    @objc func switchIsChanged(_ mySwitch: UISwitch) {
        if mySwitch.isOn {
            SearchViewController.wholesale = "true"
        } else {
            SearchViewController.wholesale = "false"
        }
    }
    
    @objc func closeTouch() {
        if let index = ShopType.listShopType.index(of: text) {
                ShopType.listShopType.remove(at: index)
        }
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                self.shopTypeCollectionView.reloadData()
            }
        }
    }
    
    @objc func applyFilter() {
        let low = Int(rangeSlider.lowerValue)
        let max = Int(rangeSlider.upperValue)
        SearchViewController.min = String(low)
        SearchViewController.max = String(max)
        SearchViewController.fshop = String(ShopType.listShopType.count)
        navigationController?.popViewController(animated: true)
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
