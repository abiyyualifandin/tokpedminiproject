//
//  ShopTypeViewController.swift
//  TokpedMiniProject
//
//  Created by Abiyyu Alifandin on 3/11/19.
//  Copyright © 2019 Abiyyu Alifandin. All rights reserved.
//

import UIKit

class ShopTypeViewController: UIViewController {

    @IBOutlet weak var goldMerchant: UIButton!
    @IBOutlet weak var goldMerchantBox: UIImageView!
    @IBOutlet weak var officialStore: UIButton!
    @IBOutlet weak var officialStoreBox: UIImageView!
    
    var gold: Bool = true
    var official: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Shop Type"
        
        if ShopType.listShopType.contains("Gold Merchant") {
            goldMerchantBox.image = UIImage(named: "check")
            gold = true
        }
        else{
            goldMerchantBox.image = UIImage(named: "uncheck")
            gold = false
        }
        
        if ShopType.listShopType.contains("Official Store"){
            officialStoreBox.image = UIImage(named: "check")
            official = true
        }
        else{
            officialStoreBox.image = UIImage(named: "uncheck")
            official = false
        }
        
        goldMerchant.addTarget(self, action: #selector(goldTouch), for: .touchUpInside)
        officialStore.addTarget(self, action: #selector(officeTouch), for: .touchUpInside)
    }
    
    @objc func goldTouch() {
        if gold == true {
            goldMerchantBox.image = UIImage(named: "uncheck")
            if ShopType.listShopType.count > 0{
                ShopType.listShopType.removeFirst()
            }
            gold = false
        }
        else{
            goldMerchantBox.image = UIImage(named: "check")
            ShopType.listShopType.append("Gold Merchant")
            gold = true
        }
    }

    @objc func officeTouch() {
        if official == true {
            officialStoreBox.image = UIImage(named: "uncheck")
            if ShopType.listShopType.count > 0{
                ShopType.listShopType.removeFirst()
            }
            official = false
        }
        else{
            officialStoreBox.image = UIImage(named: "check")
            ShopType.listShopType.append("Official Store")
            official = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
